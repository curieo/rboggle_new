class AddGameToPlayer < ActiveRecord::Migration[5.2]
  def change
    add_reference :players, :game, index: true, foreign_key: true, null: false
  end
end
