class RenameGuessValidToPossible < ActiveRecord::Migration[5.2]
  def change
    change_table :guesses do |t|
      t.rename :valid, :possible
    end
  end
end
