class AddInDictionaryToGuess < ActiveRecord::Migration[5.2]
  def change
    add_column :guesses, :in_dictionary, :boolean
  end
end
