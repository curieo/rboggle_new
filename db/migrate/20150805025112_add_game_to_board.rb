class AddGameToBoard < ActiveRecord::Migration[5.2]
  def change
    add_reference :boards, :game, index: true, foreign_key: true, null: false
  end
end
