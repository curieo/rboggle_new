class AddUniqueIndexToDictionaryWord < ActiveRecord::Migration[5.2]
  def change
    remove_index :dictionaries, :word
    add_index :dictionaries, :word, unique: true
  end
end
