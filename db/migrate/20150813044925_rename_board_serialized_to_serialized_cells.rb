class RenameBoardSerializedToSerializedCells < ActiveRecord::Migration[5.2]
  def change
    change_table :boards do |t|
      t.rename :serialized, :serialized_cells
    end
  end
end
