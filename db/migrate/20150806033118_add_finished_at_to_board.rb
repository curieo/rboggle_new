class AddFinishedAtToBoard < ActiveRecord::Migration[5.2]
  def change
    add_column :boards, :finished_at, :timestamp
  end
end
