# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2015_08_17_232331) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "boards", force: :cascade do |t|
    t.integer "round_number", null: false
    t.boolean "finished", default: false, null: false
    t.string "serialized_cells", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "game_id", null: false
    t.datetime "finished_at"
    t.index ["finished"], name: "index_boards_on_finished"
    t.index ["game_id"], name: "index_boards_on_game_id"
    t.index ["round_number"], name: "index_boards_on_round_number"
  end

  create_table "dictionaries", force: :cascade do |t|
    t.string "word", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["word"], name: "index_dictionaries_on_word", unique: true
  end

  create_table "games", force: :cascade do |t|
    t.boolean "started", default: false, null: false
    t.boolean "finished", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["finished"], name: "index_games_on_finished"
    t.index ["started"], name: "index_games_on_started"
  end

  create_table "guesses", force: :cascade do |t|
    t.bigint "player_id", null: false
    t.bigint "board_id", null: false
    t.string "word", null: false
    t.boolean "possible"
    t.boolean "checked", default: false, null: false
    t.boolean "unique"
    t.integer "score"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "in_dictionary"
    t.index ["board_id"], name: "index_guesses_on_board_id"
    t.index ["player_id"], name: "index_guesses_on_player_id"
  end

  create_table "players", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.integer "score", default: 0, null: false
    t.boolean "ready", default: false, null: false
    t.boolean "finished", default: false, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "game_id", null: false
    t.boolean "guessed", default: false, null: false
    t.index ["finished"], name: "index_players_on_finished"
    t.index ["game_id"], name: "index_players_on_game_id"
    t.index ["guessed"], name: "index_players_on_guessed"
    t.index ["ready"], name: "index_players_on_ready"
    t.index ["user_id"], name: "index_players_on_user_id"
  end

  create_table "possible_words", force: :cascade do |t|
    t.bigint "board_id", null: false
    t.string "word"
    t.bigint "dictionary_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["board_id"], name: "index_possible_words_on_board_id"
    t.index ["dictionary_id"], name: "index_possible_words_on_dictionary_id"
    t.index ["word"], name: "index_possible_words_on_word"
  end

  create_table "sessions", force: :cascade do |t|
    t.string "session_id", null: false
    t.text "data"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["session_id"], name: "index_sessions_on_session_id", unique: true
    t.index ["updated_at"], name: "index_sessions_on_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string "provider"
    t.string "uid"
    t.string "name"
    t.string "refresh_token"
    t.string "access_token"
    t.datetime "expires"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "boards", "games"
  add_foreign_key "guesses", "boards"
  add_foreign_key "guesses", "players"
  add_foreign_key "players", "games"
  add_foreign_key "players", "users"
  add_foreign_key "possible_words", "boards"
  add_foreign_key "possible_words", "dictionaries"
end
